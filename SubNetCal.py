import math

#network and host bits
def calculate_network_host_bits(ip_class, subnet_bits):
    if ip_class == 'A':
        network_bits = 8
    elif ip_class == 'B':
        network_bits = 16
    elif ip_class == 'C':
        network_bits = 24
    host_bits = 32 - network_bits - subnet_bits
    return host_bits

# subnet bits
def calculate_subnet_bits(max_subnets):
    return int(math.ceil(math.log2(max_subnets)))

# subnet bitmap
def calculate_subnet_bitmap(netclass,subnet_bits, host_bits):
    if (netclass == 'A') :
      subnet_bitmap = "0nnnnnnn"
    elif(netclass == 'B') :
      subnet_bitmap = "10nnnnnnnnnnnnnn"
    elif(netclass == 'C'):
      subnet_bitmap = "110nnnnnnnnnnnnnnnnnnnnn"

    while (subnet_bits > 0) :
      subnet_bitmap += "s"
      subnet_bits-=1
    while (host_bits > 0) :
      subnet_bitmap += "h"
      host_bits-=1
    subnet_bitmap = '.'.join([subnet_bitmap[i:i+8] for i in range(0, 32, 8)])
    return subnet_bitmap


# Integer To Bit
def Int2Bin(integer):
    binary = '.'.join([bin(int(x)+256)[3:] for x in integer.split('.')])
    return binary

# Wild Card
def complement(number):
    if number == '0':
        number = '1'
    elif number == '.':
        pass
    else:
        number = '0'
    return number

def find_wildcard(binary_subnet):
    binary_list = list(binary_subnet)
    wildcard = ''.join(complement(binary_list[y]) for y in range(len(binary_list)))
    return wildcard

# Binary To Integer(Decimal)
def convert_decimal(wildcard_Binary):
    binary = {}
    for i in range(4):
        binary[i] = int(wildcard_Binary.split(".")[i], 2)
    dec = ".".join(str(binary[i]) for i in range(4))
    return dec

# Network ID
def andOP(IP1, IP2):
    ID_list = {}
    for i in range(4):
        ID_list[i] = int(IP1.split(".")[i]) & int(IP2.split(".")[i])
    Network_ID = ".".join(str(ID_list[i]) for i in range(4))
    return Network_ID

# Broadcast IP
def orOP(IP1, IP2):
    Broadcast_list = {}
    for i in range(4):
        Broadcast_list[i] = int(IP1.split(".")[i]) | int(IP2.split(".")[i])
    broadcast = ".".join(str(Broadcast_list[i]) for i in range(4))
    return broadcast

# Max IP
def maxiIP(brdcstIP):
    maxIPs = brdcstIP.split(".")
    if int(brdcstIP.split(".")[3]) - 1 == 0:
        if int(brdcstIP.split(".")[2]) - 1 == 0:
            if int(brdcstIP.split(".")[1]) - 1 == 0:
                maxIPs[0] = int(brdcstIP.split(".")[0]) - 1 
            else:
                maxIPs[1] = int(brdcstIP.split(".")[1]) - 1 
        else:
            maxIPs[2] = int(brdcstIP.split(".")[2]) - 1 
    else:
        maxIPs[3] = int(brdcstIP.split(".")[3]) - 1 
    return ".".join(str(maxIPs[x]) for x in range(4))

# Min IP
def miniIP(ntwrkID):
    miniIPs = ntwrkID.split(".")
    if int(ntwrkID.split(".")[3]) + 1 == 256:
        if int(ntwrkID.split(".")[2]) + 1 == 256:
            if int(ntwrkID.split(".")[1]) + 1 == 256:
                miniIPs[0] = int(ntwrkID.split(".")[0]) + 1 
                miniIPs[1] = 0
                miniIPs[2] = 0
                miniIPs[3] = 0
            else:
                miniIPs[1] = int(ntwrkID.split(".")[1]) + 1 
                miniIPs[2] = 0
                miniIPs[3] = 0
        else:
            miniIPs[2] = int(ntwrkID.split(".")[2]) + 1 
            miniIPs[3] = 0
    else:
        miniIPs[3] = int(ntwrkID.split(".")[3]) + 1 
    miniIPs = [str(val) for val in miniIPs]
    return ".".join(miniIPs)

# First Octet Range
def calculate_first_octet_range(netclass):
    if (netclass == 'A'):
        return "1-126"
    elif (netclass == 'B'):
        return "128-191"
    elif (netclass == 'C'):
        return "192-223"
    else:
        return "Not in the IP Subnet Calculator"

# IP to hex
def ip_to_hex(ip_address):
    octets = ip_address.split('.')
    hex_octets = [format(int(octet), '02x').upper() for octet in octets]
    hex_ip = '.'.join(hex_octets)
    return hex_ip

# validate the network class
def validate_network_class():
  while True:
    netclass = input("Enter Network class (A, B, or C): ")
    if netclass in ['A', 'B', 'C']:
      return netclass
    else:
      print("Invalid input. Please enter A, B, or C.")

# validate an IP address
def validate_ip():
  while True:
    IP = input('Enter an IP address: ')
    octets = IP.split('.')
    if len(octets) == 4 and all(octet.isdigit() and 0 <= int(octet) <= 255 for octet in octets):
      return IP
    else:
      print("Invalid input. Please enter a valid IP address.")

# validate a subnet mask
def validate_subnet_mask():
  while True:
    Subnet = input('Enter the Subnet mask: ')
    octets = Subnet.split('.')
    if len(octets) == 4 and all(octet.isdigit() and 0 <= int(octet) <= 255 for octet in octets):
      return Subnet
    else:
      print("Invalid input. Please enter a valid Subnet mask.")

def validate_numeric_input(prompt):
    while True:
        try:
            value = int(input(prompt))
            return value
        except ValueError:
            print("Invalid input. Please enter a valid numeric value.")

netclass = validate_network_class()
IP = validate_ip()
Subnet = validate_subnet_mask()
SubnetBits = validate_numeric_input('Enter Subnet Bits: ')
MaskBits = validate_numeric_input('Enter Mask Bits: ')
MaxSubnets = validate_numeric_input('Enter Maximum Subnets: ')
HostsPerSubnet = validate_numeric_input('Enter Hosts per Subnet: ')

host_bits = calculate_network_host_bits(netclass, SubnetBits)
networkID = andOP(IP, Subnet)
FirstOctet = calculate_first_octet_range(netclass)
hexIp = ip_to_hex(IP)
WildCard = convert_decimal(find_wildcard(Int2Bin(Subnet)))
broadcastIP = orOP(networkID, WildCard)
minIP = miniIP(networkID)
maxIP = maxiIP(broadcastIP)
subnetBitsCalculated = calculate_subnet_bits(MaxSubnets)
subnet_bitmap = calculate_subnet_bitmap(netclass, subnetBitsCalculated, host_bits)

print("First Octet Range:", FirstOctet)
print('Hex IP Address:', hexIp)
print('Wildcard:', WildCard)
print('Subnet Bits:', subnetBitsCalculated)
print('Mask Bits:', MaskBits)
print('Maximum Subnets:', MaxSubnets)
print('Hosts per Subnet:', HostsPerSubnet)
print("Host Address Range: ", minIP, " - ", maxIP)
print('Subnet ID:', networkID)
print('Broadcast IP:', broadcastIP)
print('Subnet Bitmap:', subnet_bitmap)